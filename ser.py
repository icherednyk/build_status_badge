import requests

from flask import Flask, redirect


app = Flask(__name__)


@app.route("/<user>/<project>")
def index(user, project):
    url = "https://ci.appveyor.com/api/projects/" + user + "/" + project  # + "/build/1.0.117"
    json = requests.get(url).json()['build']['jobs'][0]
    status = json['status'].title()
    tests = json['testsCount']
    passed = json['passedTestsCount']
    failed = json['failedTestsCount']
    skipped = tests - passed - failed

    baseUrl = "https://img.shields.io/badge/"
    prefix = ".svg"
    color = "lightgrey"

    if status.lower() == "success":
        color = "brightgreen"
    elif status.lower() == "failed":
        color = "red"
    elif status.lower() == "running" and failed == 0:
        color = "orange"
    elif status.lower() == "running" and failed != 0:
        color = "red"
    else:
        color = "lightgrey"

    badge_url = (
        '{}{}-Total:_{},_Passed:_{},_Failed:_{},_Skipped:_{}-{}{}'.
        format(baseUrl, status, tests,
               passed, failed, skipped, color, prefix)
        )

    return redirect(badge_url)