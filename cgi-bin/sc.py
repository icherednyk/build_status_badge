#!/usr/bin/env python3

print("Content-type: text/html")
print()

import requests

username = 'icherednyk'
project = 'sourcetreeqaautomation'
build = '1.0.118'

prefix = ''

if build != '':
    prefix = '/build/' + build

url = "https://ci.appveyor.com/api/projects/" + username + "/" + project + prefix

json = requests.get(url).json()['build']['jobs'][0]
status = json['status']
tests = json['testsCount']
passed = json['passedTestsCount']
failed = json['failedTestsCount']

print('Status:', status)
print()
print('Tests:', tests)
print()
print('Passed:', passed)
print('Failed:', failed)
print('Skipped:', tests - passed - failed)